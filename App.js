import React from 'react';
import { Provider } from 'react-redux';
// import { AsyncStorage } from 'react-native';
import { PersistGate } from 'redux-persist/lib/integration/react';
import configureStore from './src/store';
import Router from './src/routers/routers.component';

const { store, persistor } = configureStore();

export default class App extends React.Component {
  // componentWillMount() {
  //   AsyncStorage.clear();
  // }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router />
        </PersistGate>
      </Provider>
    );
  }
}
