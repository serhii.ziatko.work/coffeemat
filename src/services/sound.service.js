import Expo from 'expo';

export const addCoinSound = async () => {
  const soundObject = new Expo.Audio.Sound();
  try {
    await soundObject.loadAsync(require('../../assets/sounds/addCoin.mp3'));

    await soundObject.playAsync();
    // Your sound is playing!
  } catch (error) {
    console.error(error);
  }
};

export const cookingCoffee = async () => {
  const soundObject = new Expo.Audio.Sound();
  try {
    await soundObject.loadAsync(require('../../assets/sounds/cookingCoffee.mp3'));

    await soundObject.playAsync();
  } catch (error) {
    console.error(error);
  }
};
