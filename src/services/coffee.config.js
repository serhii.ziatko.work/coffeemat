export const coffee = [
  {
    id: 1,
    name: 'Expresso',
    cost: '2',
    amount: '0.05',
    inputValue: null,
  },
  {
    id: 2,
    name: 'Cuppuccino',
    cost: '2.50',
    amount: '0.05',
    inputValue: null,
  },
  {
    id: 3,
    name: 'Irish coffee',
    cost: '1.25',
    amount: '0.3',
    inputValue: null,
  },
  {
    id: 4,
    name: 'Americano',
    cost: '2.75',
    amount: '0.2',
    inputValue: null,
  },
  {
    id: 5,
    name: 'Ristretto',
    cost: '2.10',
    amount: '0.2',
    inputValue: null,
  },
  {
    id: 6,
    name: 'Latte',
    cost: '3',
    amount: '0.3',
    inputValue: null,
  },
];

export const money = [
  {
    id: 1,
    name: '5¢',
    cost: 0.05,
    amount: 60,
    inputValue: null,
  },
  {
    id: 2,
    name: '25¢',
    cost: 0.25,
    amount: 5,
    inputValue: null,
  },
  {
    id: 3,
    name: '50¢',
    cost: 0.5,
    amount: 1,
    inputValue: null,
  },
  {
    id: 4,
    name: '1$',
    cost: 1,
    amount: 2,
    inputValue: null,
  },
];

export const stock = [
  {
    id: 1,
    name: 'Water',
    amount: 10,
    valueSign: 'L',
    inputValue: null,
  }
];

