export const text = {
  ready: {
    id: 1,
    body: '- Ready to work'
  },
  yourCash: (cash) => ({
    id: 2,
    body: `- Your cash is ${cash}$`,
  }),
  endedStock: (product) => ({
    id: 3,
    body: `- Sorry! ${product} ended`
  }),
  coffeeIsReady: (coffee) => ({
    id: 4,
    body: `- Your ${coffee} is ready`
  }),
  leave: {
    id: 5,
    body: '- Have a good day!'
  },
  cooking: {
    1: {
      id: 6,
      body: '- Cooking .'
    },
    2: {
      id: 6,
      body: '- Cooking ..'
    },
    3: {
      id: 6,
      body: '- Cooking ...'
    },
  },
  selectedCoffee: (coffee) => ({
    id: 7,
    body: `- ${coffee} is selected`
  }),

  prepareCooking: {
    id: 8,
    body: '- Prepare cooking',
  },

  takeChange: (change) => ({
    id: 9,
    body: `- Take your change ${change}$`
  }),
  dontEnoughMoney: (money) => ({
    id: 10,
    body: `- Don't enough money! Cash: ${money}$`
  }),
  dontEnoughChange: {
    id: 11,
    body: '- Sorry, we don\'t have change.',
  },
  chooseNextCoffee: {
    id: 12,
    body: '- Choose other coffee.',
  }
};
