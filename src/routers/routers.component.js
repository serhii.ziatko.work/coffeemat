import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';
import {
  Login,
  Settings,
  Dashboard,
} from '../screens';


const AppRouteConfigs = {
  login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },
  settings: {
    screen: Settings,
    navigationOptions: {
      header: null,
    }
  },
  dashboard: {
    screen: Dashboard,
    navigationOptions: {
      header: null,
    }
  },
};


export const Navigator = StackNavigator(AppRouteConfigs);

class Router extends Component {
  render() {
    return (
      <Navigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav,
        })}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  nav: state.nav
});

export default connect(mapStateToProps)(Router);

