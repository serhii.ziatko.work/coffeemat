import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import ReduxThunk from 'redux-thunk';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import reducers from '../reducers';

const config = {
  key: 'root',
  storage,
  debug: true,
  whiteList: ['auth'],
};

const reducer = persistCombineReducers(config, reducers);

const loggerMiddleware = createLogger({
  collapsed: true,
});

export default function configureStore() {
  const enhancer = compose(
    applyMiddleware(
      loggerMiddleware,
      ReduxThunk,
    ),
    global.reduxNativeDevTools ?
    global.reduxNativeDevTools(/*options*/) :
    noop => noop
  );

  const store = createStore(reducer, enhancer);
  const persistor = persistStore(store);

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/index').default;

      store.replaceReducer(nextRootReducer);
    });
  }
  if (global.reduxNativeDevTools) {
    global.reduxNativeDevTools.updateStore(store);
  }
  return { store, persistor };
}
