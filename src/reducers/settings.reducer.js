import C from '../constants';
import { stock, money, coffee } from '../services/coffee.config';

const initialState = {
  user: 'admin',
  stock,
  money,
  coffee,
};

export default function SettingsReducer(state = initialState, { type, payload }) {
  switch (type) {

    case C.CLEAR_INPUT_VALUE: {
      return {
        ...state,
        stock: state.stock.map(item => ({ ...item, inputValue: null })),
        money: state.money.map(item => ({ ...item, inputValue: null })),
        coffee: state.coffee.map(item => ({ ...item, inputValue: null })),
      };
    }

    case C.SAVE_SETTINGS: {
      return {
        ...state,
        stock: state.stock.map(item => ({
          ...item,
          amount: item.inputValue || item.amount,
          inputValue: null,
        })),
        money: state.money.map(item => ({
          ...item,
          amount: item.inputValue || item.amount,
          inputValue: null,
        })),
        coffee: state.coffee.map(item => ({
          ...item,
          cost: item.inputValue || item.cost,
          inputValue: null,
        })),
      };
    }

    case C.SET_INPUT_VALUE: {
      const { id, key, val } = payload;
      return {
        ...state,
        [key]: state[key].map((item) => {
          if (item.id === id) {
            return {
              ...item,
              inputValue: val,
            };
          }
          return item;
        })
      };
    }

    case C.SET_WATER: {
      return {
        ...state,
        stock: state.stock.map((item, i) => {
          if (i === 0) {
            return {
              ...item,
              amount: payload,
            };
          }
          return item;
        }),
      };
    }

    case C.SET_CHANGE_COINS: {
      return {
        ...state,
        money: payload,
      };
    }

    default:
      return state;
  }
}
