const initialState = {};

export default function globalReducer(state = initialState, action) {
  switch (action.type) {
    case 'REHYDRATE':
      return {
        ...state,
        persistedState: action.payload
      };

    default:
      return state;
  }
}
