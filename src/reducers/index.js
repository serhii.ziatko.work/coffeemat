import settings from './settings.reducer';
import dashboard from './dashboard.reducer';
import nav from '../routers/routers.reducer';
import main from './main.reducer';
import auth from './auth.reducer';

export default {
  settings,
  dashboard,
  nav,
  main,
  auth,
};
