import C from '../constants';

const initialState = {
  user: null,
  saveUser: false,
  mockAuth: {
    login: 'admin',
    password: 'admin',
  },
};

export default function DashboardReducer(state = initialState, { type, payload }) {
  switch (type) {

    case C.SET_USER: {
      return {
        ...state,
        user: payload,
      };
    }
    case C.CHANGE_FLAG_SAVE_USER: {
      return {
        ...state,
        saveUser: !state.saveUser
      };
    }

    default:
      return state;
  }
}
