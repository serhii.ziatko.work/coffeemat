import C from '../constants';
import { text } from '../services/display.config';

const initialState = {
  displayText: [
    text.ready,
  ],
  cash: null,
  coffee: null,
  change: 0
};

export default function DashboardReducer(state = initialState, { type, payload }) {
  switch (type) {
    case C.ADD_DISPLAY_MESSAGE: {
      return {
        ...state,
        displayText: payload,
      };
    }
    case C.CHANGE_LAST_DISPLAY_MESSAGE: {
      return {
        ...state,
        displayText: payload,
      };
    }
    case C.SET_CASH: {
      return {
        ...state,
        cash: payload,
      };
    }
    case C.SET_CHANGE: {
      return {
        ...state,
        change: payload,
      };
    }
    case C.SELECT_COFFEE: {
      return {
        ...state,
        coffee: payload,
      };
    }
    default:
      return state;
  }
}
