export const SET_WATER = 'SET_WATER';
export const SET_COFFEE = 'SET_COFFEE';
export const SET_MONEY = 'SET_MONEY';
export const SET_INPUT_VALUE = 'SET_INPUT_VALUE';
export const CLEAR_INPUT_VALUE = 'CLEAR_INPUT_VALUE';
export const SAVE_SETTINGS = 'SAVE_SETTINGS';
