import * as settings from './settings.consts';
import * as dashboard from './dashboard.consts';
import * as auth from './auth.consts';


export default Object.assign(
  {},
  settings,
  dashboard,
  auth,
);
