import React, { Component } from 'react';
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native';
import reactMixin from 'react-mixin';
import timerMixin from 'react-timer-mixin';
import { Base, LoginForm, WideButton } from '../components';

@reactMixin.decorate(timerMixin)
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.goTo = this.goTo.bind(this);
    this.submitLoginForm = this.submitLoginForm.bind(this);
    this.state = {
      showForm: false,
    };
  }

  submitLoginForm() {
    this.setState({ showForm: false });
    this.goTo('settings');
  }

  goTo(screen) {
    this.setTimeout(() => {
      this.props.navigation.navigate(screen);
    }, 0);
  }

  render() {
    const { showForm } = this.state;
    return (
      <Base>
        <KeyboardAvoidingView
          behavior='padding'
          style={[styles.content, !showForm && styles.contentModif]}
        >
          <View style={styles.buttonWrapper}>
            <WideButton
              text='Start'
              onPress={() => this.goTo('dashboard')}
            />
            <WideButton
              text='Settings'
              onPress={() => this.setState({ showForm: !showForm })}
              style={styles.buttonModif}
            />

          </View>
          {showForm &&
            <LoginForm
              onSubmit={this.submitLoginForm}
              errorMessage={this.state.errorMessage}
            /> }
        </KeyboardAvoidingView>
      </Base>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  contentModif: {
    marginBottom: 207
  },
  buttonWrapper: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  button: {
    backgroundColor: '#f3d475',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginBottom: 20,
    borderRadius: 2,
  },
  buttonModif: {
    backgroundColor: '#84b0f1',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
  },

});
