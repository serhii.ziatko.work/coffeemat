import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Base, Display, InputDashboard, Key, DeliveryWindow, SmallButton } from '../components';
import A from '../actions';
import { text } from '../services/display.config';
import { addCoinSound, cookingCoffee } from '../services/sound.service';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.goTo = this.goTo.bind(this);
    this.goBack = this.goBack.bind(this);
    this.handleCash = this.handleCash.bind(this);
    this.handleCoffeeSelect = this.handleCoffeeSelect.bind(this);
    this.handleCountChange = this.handleCountChange.bind(this);
    this.state = {
      change: 0,
      disabled: true,
      countChange: null,
    };
  }

  setChange(change) {
    this.setState({ change });
  }

  takeChangeBtnState(disabled) {
    this.setState({ disabled });
  }

  // handlers
  handleCash(inputText) {
    this.props.onSetCash(inputText);
    this.display.changeLastMessage(text.yourCash(inputText));
    addCoinSound();
  }

  handleCoffeeSelect(coffee) {
    this.props.onSelectCoffee(coffee);
    if (this.checkResources(coffee)) {
      this.display.addNextMessage(text.selectedCoffee(coffee.name));
      this.setNewAmountWater(coffee);
      this.handleCooking();
    } else {
      this.props.onSelectCoffee(null);
    }
  }

  async handleCooking() {
    await this.prepareCooking();
    await this.cooking();
    await this.finishCooking();
  }

  handleCountChange() {
    const countChange = this.state.countChange;
    if (countChange.result) {
      this.setChange(this.cash);
      if (this.state.change) {
        this.display.addNextMessage(text.takeChange(this.state.change));
      }
    } else {
      this.display.addNextMessage(text.dontEnoughChange);
    }
  }

  handleReceiveChange() {
    this.props.onChangeCoinsForChange(this.state.countChange.changeMoney);
    this.props.onSetCash(null);
    this.setChange(0);
    this.setState({ countChange: null });
    this.takeChangeBtnState(true);
    addCoinSound();
  }

  countBalance() {
    const balance = +((+this.cash - +this.coffee.cost).toFixed(2));
    this.props.onSetCash(balance);
  }

  setNewAmountWater(coffee) {
    const water = this.props.settings.stock[0];
    const range = (+water.amount - +coffee.amount).toFixed(2);
    this.props.onSetWater(range);
  }

  // cooking
  prepareCooking() {
    return new Promise((res) => {
      this.takeChangeBtnState(true);
      this.delivWindow.onDisappearCoffee(300);
      setTimeout(() => {
        this.delivWindow.onAppearCup();
        this.display.addNextMessage(text.prepareCooking);
        res();
      }, 500);
    });
  }

  cooking() {
    return new Promise((res) => {
      let step = 1;
      this.display.changeLastMessage(text.cooking[step]);
      const timer = setInterval(() => {
        this.display.changeLastMessage(text.cooking[step]);
        if (step === 3) step = 1;
          else step += 1;
      }, 1000);
      cookingCoffee();
      setTimeout(() => {
        clearInterval(timer);
        res();
      }, 35000);
    });
  }

  finishCooking() {
    return new Promise((res) => {
      setTimeout(() => {
        this.delivWindow.onAppearSmoke();
        this.display.addNextMessage(text.coffeeIsReady(this.props.dashboard.coffee.name));
        this.countBalance();
        this.countChange();
        this.handleCountChange();
        const { cash, result } = this.state.countChange;
        this.takeChangeBtnState(!result && cash !== 0);
      }, 300);
      setTimeout(() => {
        if (this.state.countChange.result) {
          this.display.addNextMessage(text.leave);
        } else {
          this.display.addNextMessage(text.chooseNextCoffee);
        }
        this.props.onSelectCoffee(null);
        res();
      }, 600);
    });
  }

  // checkings
  checkResources(coffee) {
    if (!this.isEnoughMoney(coffee)) {
      this.display.addNextMessage(text.dontEnoughMoney(this.cash === null ? 0 : this.cash));
      return false;
    }
    if (!this.isEnoughWater(coffee)) {
      this.display.addNextMessage(text.endedStock('Water'));
      return false;
    }
    return true;
  }

  isEnoughWater(coffee) {
    const water = this.props.settings.stock[0];
    const range = +water.amount - +coffee.amount;
    return range >= 0;
  }

  isEnoughMoney(coffee) {
    if (this.cash === null) {
      return false;
    }
    const range = +this.cash - +coffee.cost;
    return range >= 0;
  }

  countChange() {
    let cash = this.cash;
    const changeMoney = this.props.settings.money.map(item => Object.assign({}, item));
    if (cash === 0) {
      this.setState({
        countChange: {
          result: true,
          cash,
          changeMoney,
        }
      });
      return;
    }
    for (let i = changeMoney.length - 1; i >= 0; i--) {
      if (cash === 0) break;
      const coin = changeMoney[i];
      for (let k = coin.amount; k > 0; k--) {
        if (cash < coin.cost) break;
        cash = +((cash - coin.cost).toFixed(2));
        --changeMoney[i].amount;
      }
    }
    if (cash === 0) {
      this.setState({
        countChange: {
          result: true,
          cash,
          changeMoney,
        }
      });
      return;
    }
    if (cash < 0.05) {
      this.setState({
        countChange: {
          result: false,
          cash,
          changeMoney,
        }
      });
    } else {
      this.setState({
        countChange: {
          result: false,
          cash,
          changeMoney,
        }
      });
      return;
    }
  }

  // navigation
  goTo(screen) { this.props.navigation.navigate(screen); }

  goBack() { this.props.navigation.goBack(); }

  // getters
  get cash() { return this.props.dashboard.cash; }

  get coffee() { return this.props.dashboard.coffee; }

  render() {
    const { coffee } = this.props.settings;
    const { cash, displayText } = this.props.dashboard;
    return (
      <Base goBack={this.goBack}>
        <View style={styles.container}>

          {/* display */}
          <View style={styles.displayZone}>
            <Display
              ref={child => { this.display = child; }}
              displayText={displayText}
              onAddDisplayMsg={this.props.onAddDisplayMsg}
            />
            <View style={styles.cashZone}>
              <Text style={styles.cashText}>Money:</Text>
              <InputDashboard
                inputValue={cash ? cash.toString() : ''}
                style={styles.cashInput}
                onChangeText={this.handleCash}
              />
            </View>
          </View>

          {/* keyboard */}
          <View style={styles.keyboardZone}>
            {coffee.map((item, i) => (
              <Key
                key={item.id}
                coffee={item}
                paddingLeft={!!(i % 2)}
                onPress={() => this.handleCoffeeSelect(item)}
                disabled={!!this.coffee}
              />
            ))}
          </View>

          {/* delivery window */}
          <View style={styles.deliveryZone}>
            <DeliveryWindow ref={child => { this.delivWindow = child; }} {...this.props} />
            <View style={styles.cashZone}>
              <Text style={[styles.cashText, styles.changeText]}>Take your change:</Text>
              <SmallButton
                disabled={this.state.disabled}
                style={styles.changeButton}
                text={this.state.change ? `${this.state.change}$` : '_'}
                onPress={() => this.handleReceiveChange()}
              />
            </View>
          </View>

        </View>
      </Base>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onAddDisplayMsg: (msgs) => dispatch(A.addDisplayMsg(msgs)),
    onChangeLastDisplayMsg: (msgs) => dispatch(A.changeLastDisplayMsg(msgs)),
    onSetCash: (cash) => dispatch(A.setCash(cash)),
    onSelectCoffee: (cash) => dispatch(A.selectCoffee(cash)),
    onSetWater: (water) => dispatch(A.setWater(water)),
    onChangeCoinsForChange: (coins) => dispatch(A.setChangeCoins(coins)),
  };
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    dashboard: state.dashboard,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  displayZone: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    paddingHorizontal: 10,
    paddingBottom: 17,
  },
  cashZone: {
    flex: 1,
    marginLeft: 20,
    justifyContent: 'flex-end',
  },
  cashText: {
    fontSize: 14,
    color: '#756664',
    marginBottom: 8,
    lineHeight: 24
  },
  cashInput: {
    width: '100%',
  },
  keyboardZone: {
    padding: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#b4b4b4',
    backgroundColor: '#ebe8e0',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  deliveryZone: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    flexWrap: 'nowrap',
    flexDirection: 'row',
  },
  changeText: {
    width: '70%'
  },
  changeButton: {
    width: '70%'
  }
});
