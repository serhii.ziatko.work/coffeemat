import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import { Base, ListItemWide, WideButton, ListItemShort, KeyboardAvoid } from '../components';
import A from '../actions';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.goTo = this.goTo.bind(this);
    this.goBack = this.goBack.bind(this);
    this.sumCents = this.sumCents.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
    this.clearInputs = this.clearInputs.bind(this);
    this.saveSettings = this.saveSettings.bind(this);
    this.focusEvent = this.focusEvent.bind(this);
    this.refCounter = this.refCounter.bind(this);
    this.countMoneyTotal = this.countMoneyTotal.bind(this);
    this.count = 0;
  }

  setInputValue(id, val, key) {
    this.props.onSetInputValue({ id, val, key });
  }

  clearInputs() {
    this.props.onClearInputValue();
  }

  saveSettings() {
    this.props.onSaveSettings();
  }

  goTo(screen) { this.props.navigation.navigate(screen); }

  goBack() { this.props.navigation.goBack(); }

  sumCents(value, amount) {
    return +((value * amount).toFixed(2));
  }

  focusEvent(input) {
    // this.child.handleKeyboard(input);
    console.log(input);
  }

  refCounter() {
    return ++this.count;
  }

  countMoneyTotal() {
    const coins = this.props.settings.money;
    let summTotal = 0;
    coins.forEach((item) => {
      summTotal += +((item.cost * item.amount).toFixed(2));
    });
    return summTotal;
  }

  render() {
    const { money, coffee, stock } = this.props.settings;
    return (
      <Base goBack={this.goBack}>
        <KeyboardAvoid>
          <View style={styles.container}>

            {/* Stock */}
            {stock.map((el) => (
              <ListItemWide
                key={el.id}
                title={el.name}
                titleStyle={styles.bold}
                value={el.amount}
                valueStyle={styles.blue}
                valueSign={el.valueSign}
                style={styles.divider}
                inputValue={el.inputValue}
                onChangeText={(val) => this.setInputValue(el.id, val, 'stock')}
              />
            ))}
            {/* Money total */}
            <View style={styles.listItem}>
              <Text style={[styles.text, styles.bold]}>Money total:</Text>
              <Text style={[styles.text, styles.bold, styles.green]}>
                {this.countMoneyTotal()}$
              </Text>
            </View>

            {/* Money items */}
            <View style={[styles.moneyWrapper]}>
              {money.map((el, i) => (
                <ListItemShort
                  title={el.name}
                  value={this.sumCents(el.cost, el.amount)}
                  key={el.id}
                  paddingLeft={!!(i % 2)}
                  style={styles.divider}
                  inputValue={el.inputValue}
                  onChangeText={(val) => this.setInputValue(el.id, val, 'money')}
                />))
              }
            </View>

            {/* coffee */}
            <View style={styles.divider}>
              {coffee.map(el => (
                <ListItemWide
                  title={el.name}
                  value={el.cost}
                  key={el.id}
                  inputValue={el.inputValue}
                  onChangeText={(val) => this.setInputValue(el.id, val, 'coffee')}
                />))}
            </View>

            {/* button */}
            <View style={styles.buttonsWrapper}>
              <WideButton
                text='Clear'
                onPress={this.clearInputs}
                style={[styles.buttons, styles.buttonModif]}
              />
              <WideButton
                text='Save'
                onPress={this.saveSettings}
                style={styles.buttons}
              />
            </View>

            </View>
          </KeyboardAvoid>
      </Base>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSetUser: (payload) => dispatch(A.setUser(payload)),
    onSetMoney: (payload) => dispatch(A.setMoney(payload)),
    onSetCoffee: (payload) => dispatch(A.setCoffee(payload)),
    onSetWater: (payload) => dispatch(A.setWater(payload)),
    onSetInputValue: (payload) => dispatch(A.setInputValue(payload)),
    onClearInputValue: () => dispatch(A.clearInputValue()),
    onSaveSettings: () => dispatch(A.saveSettings()),
  };
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  listItem: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#dcd9d8',
    paddingVertical: 5,
  },
  text: {
    color: '#756664',
    paddingRight: 4,
  },
  bold: {
    fontWeight: '800',
  },
  blue: {
    color: '#84b0f1'
  },
  green: {
    color: '#8fdb7b'
  },
  brown: {
    color: '#9b6c6c'
  },
  divider: {
    marginBottom: 15,
  },

  moneyWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  buttonsWrapper: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
  },
  buttons: {
    width: '49%',
    marginBottom: 0,
  },
  buttonModif: {
    backgroundColor: '#84b0f1',
  },

  keyboard: {
    paddingBottom: 100,
    position: 'absolute',
  }
});
