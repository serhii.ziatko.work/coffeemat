import C from '../constants';

export function setUser(payload) {
  return {
    type: C.SET_USER,
    payload,
  };
}

export function changeFlagSaveUser() {
  return {
    type: C.CHANGE_FLAG_SAVE_USER,
  };
}

