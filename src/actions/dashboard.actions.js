import C from '../constants';

export function addDisplayMsg(payload) {
  return {
    type: C.ADD_DISPLAY_MESSAGE,
    payload,
  };
}

export function changeLastDisplayMsg(payload) {
  return {
    type: C.CHANGE_LAST_DISPLAY_MESSAGE,
    payload,
  };
}

export function setCash(payload) {
  return {
    type: C.SET_CASH,
    payload,
  };
}

export function setChange(payload) {
  return {
    type: C.SET_CHANGE,
    payload,
  };
}

export function selectCoffee(payload) {
  return {
    type: C.SELECT_COFFEE,
    payload,
  };
}

