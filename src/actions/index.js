import * as settings from './settings.actions';
import * as dashboard from './dashboard.actions';
import * as auth from './auth.actions';


export default Object.assign(
  {},
  settings,
  dashboard,
  auth,
);
