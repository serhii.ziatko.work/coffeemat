import C from '../constants';

export function setMoney(payload) {
  return {
    type: C.SET_MONEY,
    payload,
  };
}

export function setCoffee(payload) {
  return {
    type: C.SET_COFFEE,
    payload,
  };
}

export function setWater(payload) {
  return {
    type: C.SET_WATER,
    payload,
  };
}

export function setInputValue(payload) {
  return {
    type: C.SET_INPUT_VALUE,
    payload,
  };
}

export function clearInputValue() {
  return {
    type: C.CLEAR_INPUT_VALUE,
  };
}

export function saveSettings() {
  return {
    type: C.SAVE_SETTINGS,
  };
}

export function setChangeCoins(payload) {
  return {
    type: C.SET_CHANGE_COINS,
    payload
  };
}

