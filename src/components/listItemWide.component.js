import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Input } from '../components';

export default class ListItemWide extends Component {
  render() {
    const { title, titleStyle, value, valueStyle, valueSign, style, inputValue, onChangeText } = this.props;
    return (
    <View style={[styles.listItem, style]}>
      <View style={styles.textWrapper}>
        <Text
          style={[styles.text, titleStyle]}
        >{title}:</Text>
        <Text
          style={[styles.text, styles.bold, styles.brown, valueStyle]}
        >{value}{valueSign || '$'}</Text>
      </View>
      <Input inputValue={inputValue} onChangeText={onChangeText} />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#dcd9d8',
    paddingVertical: 5,
  },
  text: {
    color: '#756664',
    paddingRight: 4,
  },
  textWrapper: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    paddingRight: 10,
  },
  bold: {
    fontWeight: '800',
  },
  brown: {
    color: '#9b6c6c'
  },
});
