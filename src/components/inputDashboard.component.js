import React, { Component } from 'react';
import { StyleSheet, TextInput } from 'react-native';

export default class InputDashboard extends Component {
  render() {
    const { style, onChangeText, inputValue, editable } = this.props;
    return (
      <TextInput
        editable={editable}
        underlineColorAndroid='transparent'
        style={[styles.input, style]}
        onChangeText={onChangeText}
        keyboardType='numeric'
        maxLength={4}
        value={inputValue}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 26,
    borderWidth: 1,
    borderColor: '#dcd9d8',
    backgroundColor: '#ebe8e0',
    color: '#756664',
    paddingHorizontal: 10,
    width: '25%',
    fontWeight: 'bold',
    textDecorationLine: 'none',
  }
});
