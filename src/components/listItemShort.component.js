import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Input } from '../components';

export default class ListItemShort extends Component {
  render() {
    const { title, value, style, paddingLeft, inputValue, onChangeText } = this.props;
    return (
      <View
        style={[
          styles.listItem,
          paddingLeft ? styles.paddingLeft : styles.paddingRight,
          style,
        ]}
      >
      <View style={styles.textWrapper}>
        <Text
          style={[styles.text]}
        >{title}:</Text>
        <Text
          style={[styles.text, styles.bold, styles.green]}
        >{value}$</Text>
      </View>
        <Input
          style={styles.input}
          inputValue={inputValue}
          onChangeText={onChangeText}
        />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#dcd9d8',
    paddingVertical: 5,
    width: '50%',
  },
  text: {
    color: '#756664',
    paddingRight: 4,
  },
  textWrapper: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    paddingRight: 10,
  },
  bold: {
    fontWeight: '800',
  },
  green: {
    color: '#8fdb7b'
  },

  input: {
    width: '50%'
  },

  paddingLeft: {
    paddingLeft: 2.5,
  },
  paddingRight: {
    paddingRight: 2.5,
  }
});
