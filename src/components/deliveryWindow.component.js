import React, { Component } from 'react';
import { StyleSheet, View, Animated, Easing, TouchableWithoutFeedback } from 'react-native';
import reactMixin from 'react-mixin';
import timerMixin from 'react-timer-mixin';

@reactMixin.decorate(timerMixin)
export default class DeliveryWindow extends Component {
  constructor(props) {
    super(props);

    this.appearCupValue = new Animated.Value(0);
    this.appearSmokeValue = new Animated.Value(0);
    this.state = {
      disabled: true,
      coffeeShowed: false,
    };
  }

  onAppearCup() {
    if (this.state.coffeeShowed) {
      return;
    }
    this.appearCupValue.setValue(0);
    this.requestAnimationFrame(() => {
      Animated.timing(
        this.appearCupValue,
        {
          toValue: 1,
          duration: 1000,
          easing: Easing.linear,
          useNativeDriver: true,
        }
      ).start();
    });
    this.setState({ disabled: false, coffeeShowed: true });
  }

  onAppearSmoke() {
    this.appearSmokeValue.setValue(0);
    this.requestAnimationFrame(() => {
      Animated.timing(
        this.appearSmokeValue,
        {
          toValue: 1,
          duration: 1000,
          easing: Easing.linear,
          useNativeDriver: true,
        }
      ).start();
    });
  }

  onDisappearCoffee(duration) {
    if (!this.state.coffeeShowed) {
      return;
    }
    this.appearCupValue.setValue(1);
    this.appearSmokeValue.setValue(1);
    this.requestAnimationFrame(() => {
      Animated.parallel([
        Animated.timing(
          this.appearCupValue,
          {
            toValue: 0,
            duration,
            easing: Easing.linear,
            useNativeDriver: true,
          }
        ),
        Animated.timing(
          this.appearSmokeValue,
          {
            toValue: 0,
            duration,
            easing: Easing.linear,
            useNativeDriver: true,
          }
        )
      ]).start();
    });
    this.setState({ disabled: true, coffeeShowed: false });
  }

  render() {
    const appearCup = this.appearCupValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    });
    const appearSmoke = this.appearSmokeValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    });
    return (
      <View
        style={styles.window}
      >
        <TouchableWithoutFeedback
          onPress={this.onDisappearCoffee.bind(this, 1000)}
          disabled={this.state.disabled}
        >
          <View>
            <View style={[styles.smoke]}>
              <Animated.Image
                style={[styles.icon, { opacity: appearSmoke }]}
                source={require('../../assets/images/cup02.png')}
              />
            </View>
            <View style={[styles.cup]}>
              <Animated.Image
                style={[styles.icon, { opacity: appearCup }]}
                source={require('../../assets/images/cup01.png')}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  window: {
    backgroundColor: '#dbd7cf',
    width: '60%',
    padding: 10,
    borderRadius: 5,
    height: 110,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  smoke: {
    width: 50,
    aspectRatio: 2.275
  },
  cup: {
    width: 50,
    aspectRatio: 0.8125
  },
  icon: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  }
});
