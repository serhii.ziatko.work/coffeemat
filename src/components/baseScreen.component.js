import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Constants } from 'expo';

export default class Base extends Component {
  goBack = () => {
    this.props.goBack();
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusBar} />
        <View style={styles.header}>

          <View style={styles.headerBackgroundWrapper}>
            <Image
              style={styles.headerBackground}
              source={require('../../assets/images/bg.png')}
            />
          </View>
          {this.props.goBack ? (
            <TouchableOpacity
              onPress={this.goBack}
              style={styles.headerBackArrowWrapper}
            >
              <Image
                style={styles.headerBackArrow}
                source={require('../../assets/images/back.png')}
              />
            </TouchableOpacity>
          ) : null}
          <View style={styles.headerLogoWrapper}>
            <Image
              style={styles.headerLogo}
              source={require('../../assets/images/logo-text.png')}
            />
          </View>
        </View>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f5f3f1',
  },
  statusBar: {
    height: Constants.statusBarHeight,
    backgroundColor: '#edece7',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 16,
    paddingBottom: 10,
    position: 'relative',
    aspectRatio: 4.76,
  },
  headerLogoWrapper: {
    width: '30%',
    aspectRatio: 6.59,
  },
  headerLogo: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  headerBackArrowWrapper: {
    position: 'absolute',
    left: 20,
    top: 11,
    width: 25,
    height: 25,
  },
  headerBackArrow: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  headerBackgroundWrapper: {
    position: 'absolute',
    width: '100%',
    aspectRatio: 4.76,
  },
  headerBackground: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
});
