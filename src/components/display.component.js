import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';


export default class Display extends Component {
  addNextMessage(msg) {
    const { displayText } = this.props;
    if (displayText.length === 3) {
      displayText.shift();
    }
    displayText.push(msg);
    this.props.onAddDisplayMsg(displayText);
  }

  changeLastMessage(newMsg) {
    const { displayText } = this.props;
    const lastIndex = displayText.length - 1;
    if (displayText[lastIndex].id === newMsg.id) {
      displayText[lastIndex] = newMsg;
      this.props.onAddDisplayMsg(displayText);
    } else {
      this.addNextMessage(newMsg);
    }
  }

  render() {
    const { displayText } = this.props;
    return (
      <View
        style={styles.display}
      >
        {displayText.map((text, i) => (
          <Text
            style={styles.displayText}
            ellipsizeMode='tail'
            numberOfLines={1}
            key={i}
          >{text.body}</Text>))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  display: {
    backgroundColor: '#84b0f1',
    width: '70%',
    padding: 10,
    borderRadius: 5,
    height: 75,
  },
  displayText: {
    color: 'white',
  }
});
