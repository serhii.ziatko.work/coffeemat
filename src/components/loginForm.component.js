import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import A from '../actions';
import { WideButton } from '../components';

class LoginForm extends Component {

  state = {
    value: {
      login: null,
      password: null,
    },
    isValid: false,
    errorMessage: null,
  };

  componentDidMount() {
    this.checkSavedUser();
    this.onCheckValid();
  }

  onCheckValid = () => {
    const { login, password } = this.state.value;
    if (login && password) {
      this.setState({ isValid: true });
    }
  }

  onChangeValue = (key, value) => {
    this.setState({
      value: {
        ...this.state.value,
        [key]: value
      }
    });
    this.onCheckValid();
  }

  onSubmit = () => {
    const { value } = this.state;
    if (this.checkAuth(value)) {
      this.props.onSubmit();
      this.props.onSetUser(value);
      return;
    }
    this.setState({ errorMessage: 'Login or Password is wrong! Please try again.' });
  }

  checkSavedUser = () => {
    if (this.props.auth.saveUser) {
      const { login, password } = this.props.auth.user;
      this.setState({
        value: {
          login,
          password,
        }
      });
      this.onCheckValid();
    }
  }

  checkAuth = (value) => {
    const { login, password } = this.props.auth.mockAuth;
    if (value.login.trim() === login && value.password.trim() === password) {
      return true;
    }
    return false;
  }

  render() {
    const { value } = this.state;
    return (
      <View style={styles.settingsFormWrapper}>
        <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Login:</Text>
          <TextInput
            underlineColorAndroid='transparent'
            returnKeyType='next'
            autoFocus
            value={this.state.value.login}
            style={styles.input}
            blurOnSubmit={false}
            onChangeText={(text) => this.onChangeValue('login', text)}
            onSubmitEditing={() => {
              this.refs.SecondInput.focus();
            }}
          />
        </View>
        <View style={styles.inputWrapper}>
          <Text style={styles.label}>Password:</Text>
          <TextInput
            ref='SecondInput'
            underlineColorAndroid='transparent'
            returnKeyType='send'
            value={this.state.value.password}
            secureTextEntry
            style={styles.input}
            onChangeText={(text) => this.onChangeValue('password', text)}
            onSubmitEditing={this.onSubmit}
          />
        </View>
        <View style={[styles.inputWrapper, styles.checkboxWrapper]}>
          <CheckBox
            isChecked={this.props.auth.saveUser}
            onClick={this.props.onChangeFlagSaveUser}
            rightTextView={<Text style={styles.label}>Save me</Text>}
            checkBoxColor="#84b0f1"
          />
        </View>
        <WideButton
          disabled={!(value.login && value.password)}
          onPress={this.onSubmit}
          text='OK'
        />
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSetUser: (user) => dispatch(A.setUser(user)),
    onChangeFlagSaveUser: () => dispatch(A.changeFlagSaveUser()),
  };
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    auth: state.auth,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

const styles = StyleSheet.create({
  settingsFormWrapper: {
    width: '100%',
    paddingHorizontal: 10,
    paddingTop: 20,
    backgroundColor: '#edece7',
    position: 'relative',
  },
  label: {
    fontSize: 14,
    color: '#756664',
    marginBottom: 3.5,
  },
  input: {
    height: 26,
    borderWidth: 1,
    borderColor: '#dcd9d8',
    backgroundColor: '#ebe8e0',
    color: '#756664',
    paddingHorizontal: 10,
  },
  inputWrapper: {
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#f3d475',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginBottom: 20,
    borderRadius: 2,
  },
  buttonDisabled: {
    backgroundColor: '#BDBDBD'
  },
  errorMessage: {
    position: 'absolute',
    right: 10,
    top: 5,
    fontSize: 12,
    color: 'red',
  },
  checkboxWrapper: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'center',
  },
});
