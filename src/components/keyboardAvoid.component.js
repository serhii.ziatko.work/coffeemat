import React, { Component } from 'react';
import { View, Keyboard, ScrollView } from 'react-native';
import NativeMethodsMixin from 'NativeMethodsMixin';

export default class KeyboardAvoid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputPosition: null,
    };
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
    this._view = null;
    this._viewHeight = null;
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(e) {
    NativeMethodsMixin.measure.call(this.refs.view, (x, y, width, height) => {
      this._viewHeight = height;
      const padding = height - e.endCoordinates.height - 7;
      this.setState({ padding });
    });
  }

  _keyboardDidHide() {
    this.setState({ padding: this._viewHeight });
  }

  render() {
    return (
      <View>
        <ScrollView
          ref='view'
          style={{ height: this.state.padding }}
          keyboardShouldPersistTaps='handled'
        >
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}

