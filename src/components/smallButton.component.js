import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export default class SmallButton extends Component {
  render() {
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        style={[styles.button, this.props.style, this.props.disabled && styles.buttonDisabled]}
        onPress={this.props.onPress}
      >
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#84b0f1',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    marginBottom: 0,
    borderRadius: 2,
  },
  buttonDisabled: {
    backgroundColor: '#BDBDBD'
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
});
