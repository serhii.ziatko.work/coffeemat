import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';

export default class Key extends Component {
  render() {
    const { onPress, coffee, style, paddingLeft, disabled } = this.props;
    return (
      <View
        style={[styles.key, paddingLeft && styles.paddingLeft, style]}
      >
        <TouchableOpacity
          style={styles.keyBtn}
          onPress={onPress}
          disabled={disabled}
        >
          <Image
            style={styles.keyIcon}
            source={require('../../assets/images/button.png')}
          />
        </TouchableOpacity>
        <View style={styles.keyTextWrapper}>
          <Text style={styles.keyTextName}>{(coffee.name).toUpperCase()}</Text>
          <Text style={styles.keyTextCost}>{coffee.cost}$</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  key: {
    width: '50%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    paddingRight: 5,
    paddingVertical: 10,
  },
  keyBtn: {
    width: 40,
    height: 40,
    marginRight: 10,
  },
  keyIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  paddingLeft: {
    paddingLeft: 5
  },
  keyTextWrapper: {

  },
  keyTextName: {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '600',
    color: '#756664'
  },
  keyTextCost: {
    fontSize: 18,
    fontStyle: 'italic',
    fontWeight: '600',
    color: '#8fdb7b'
  }
});
